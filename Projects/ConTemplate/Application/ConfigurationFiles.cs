﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConTemplate.Application
{
    public static class ConfigurationFiles
    {
        private const string TemplateFileExtension = ".template";

        public static void Generate(Configuration configuration)
        {
            var templateFiles = IO.File.GetList(configuration.RootTemplateSearchFolder, TemplateFileExtension, true);

            var entries = ConfigurationEntries.Parse(configuration.EnvironmentConfigEntriesFile);

            foreach (var templateFile in templateFiles)
            {
                var configPath = Path.GetDirectoryName(templateFile);

                if (configPath == null)
                {
                    continue;
                }

                var configFile = Path.Combine(configPath, Path.GetFileNameWithoutExtension(templateFile));

                try
                {
                    if (File.Exists(configFile))
                    {
                        File.Delete(configFile);
                    }
                }
                catch (Exception exception)
                {
                    Logging.Logger.Debug("Error deleting config file: Error: {0}", exception.Message);
                    Logging.Logger.Error("Error deleting config file, config not generated: {0}", configFile);
                    continue;
                }

                var templateContent = IO.File.ToString(templateFile, false);

                var templateTokens = Regex.Matches(templateContent, @"(\{{[A-Z,a-z,0-9,.,_]+\}})")
                    .Cast<Match>()
                    .Select(m => m.Value)
                    .ToList();

                if (templateTokens.Count <= 0)
                {
                    Logging.Logger.Warn("There are no config variables in the template: {0}", configFile);
                }

                var configContent = templateContent;

                foreach (var entry in entries)
                {
                    var value = entry.Value;

                    if (!String.IsNullOrWhiteSpace(configuration.PathRoot))
                    {
                        if (value.StartsWith(@"~\"))
                        {
                            value = value.Remove(0, 2);
                            value = Path.Combine(configuration.PathRoot, value);
                        }
                    }

                    configContent = configContent.Replace(String.Concat("{{", entry.Key, "}}"), value);
                }

                var configTokens = Regex.Matches(configContent, @"(\{{[A-Z,a-z,0-9,.,_]+\}})")
                    .Cast<Match>()
                    .Select(m => m.Value)
                    .ToList();

                if (configTokens.Count > 0)
                {
                    foreach (var token in configTokens)
                    {
                        Logging.Logger.Warn("{0} has not been set in {1}", token, configFile);
                    }
                }

                try
                {
                    File.WriteAllText(configFile, configContent);
                    Logging.Logger.Info("Config file generated: {0}", configFile);
                }
                catch (Exception exception)
                {
                    Logging.Logger.Debug("Error writing config file: {0}, Error: {1}", configFile, exception.Message);
                    Logging.Logger.Error("Error writing config file: {0}", configFile);
                }
            }
        }
    }
}