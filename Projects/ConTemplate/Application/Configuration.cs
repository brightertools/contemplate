﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConTemplate.Application
{
    public class Configuration
    {
        #region Constants

        private const string PathRootCliKey = "-pathroot";
        private const string EnviromentCliKey = "-env";
        private const string ConfigEntriesCliKey = "-entries";
        private const string RootTemplateSearchCliKey = "-search";
        private const string ConfigEntriesFileExtension = ".entries";

        #endregion

        #region Properties

        public string PathRoot { get; private set; }
        private string ConfigEntriesFolder { get; set; }
        public string RootTemplateSearchFolder { get; private set; }
        private string Environment { get; set; }
        public string EnvironmentConfigEntriesFile { get; private set; }

        #endregion

        #region Constructor

        public Configuration(IList<string> commandArguments)
        {
            PathRoot = String.Empty;
            ConfigEntriesFolder = String.Empty;
            RootTemplateSearchFolder = String.Empty;
            Environment = String.Empty;
            EnvironmentConfigEntriesFile = String.Empty;

            for (var index = 0; index < commandArguments.Count; index++)
            {
                if (commandArguments[index] == ConfigEntriesCliKey)
                {
                    if (commandArguments.Count > index)
                    {
                        ConfigEntriesFolder = commandArguments[index + 1];
                    }
                }

                if (commandArguments[index] == RootTemplateSearchCliKey)
                {
                    if (commandArguments.Count > index)
                    {
                        RootTemplateSearchFolder = commandArguments[index + 1];
                    }
                }

                if (commandArguments[index] == EnviromentCliKey)
                {
                    if (commandArguments.Count > index)
                    {
                        Environment = commandArguments[index + 1];
                    }
                }

                if (commandArguments[index] == PathRootCliKey)
                {
                    if (commandArguments.Count > index)
                    {
                        PathRoot = commandArguments[index + 1];
                    }
                }
            }
        }

        #endregion

        #region Public Methods

        public bool Validate()
        {
            var result = true;

            if (String.IsNullOrWhiteSpace(Environment))
            {
                Environment = System.Environment.MachineName;
            }

            if (String.IsNullOrWhiteSpace(RootTemplateSearchFolder))
            {
                Logging.Logger.Error("Root template search Folder ({0}) not specified", RootTemplateSearchCliKey);
                result = false;
            }
            else if (!Directory.Exists(RootTemplateSearchFolder))
            {
                Logging.Logger.Error("Root template search folder ({0}) does not exist", RootTemplateSearchCliKey);
                result = false;
            }

            if (String.IsNullOrWhiteSpace(ConfigEntriesFolder))
            {
                Logging.Logger.Error("Configuration entries folder ({0}) not specified", ConfigEntriesCliKey);
                result = false;
            }
            else if (!Directory.Exists(ConfigEntriesFolder))
            {
                Logging.Logger.Error("Configuration entries folder ({0}) does not exist", ConfigEntriesCliKey);
                result = false;
            }
            else
            {
                var entriesFile = Path.Combine(ConfigEntriesFolder, String.Concat(Environment, ConfigEntriesFileExtension));

                if (!File.Exists(entriesFile))
                {
                    Logging.Logger.Error("Configuration entries file not found ({0})", entriesFile);
                    result = false;
                }
                else
                {
                    EnvironmentConfigEntriesFile = Path.Combine(ConfigEntriesFolder,
                        String.Concat(Environment, ConfigEntriesFileExtension));
                }
            }

            return result;
        }

        #endregion
    }
}