﻿using System;
using System.Collections.Generic;

namespace ConTemplate.Application
{
    public static class ConfigurationEntries
    {
        private const string EntryDelimiter = ":";
        private const string EntryCommentIndicator = "#";

        public static Dictionary<string, string> Parse(string filePath)
        {
            Logging.Logger.Trace("Method Entry, ConfigEntries.Parse: {0}", filePath);

            var result = new Dictionary<string, string>();

            using (var file = System.IO.File.OpenText(filePath))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    if (String.IsNullOrWhiteSpace(line) || line.Trim().StartsWith(EntryCommentIndicator) || !line.Contains(EntryDelimiter))
                    {
                        continue;
                    }

                    var entry = SplitEntry(line);

                    if (String.IsNullOrWhiteSpace(entry.Key))
                    {
                        continue;
                    }

                    if (result.ContainsKey(entry.Key))
                    {
                        Logging.Logger.Warn("Duplicate entry found: {0}, first value will be used", entry.Key);
                        continue;
                    }

                    if (String.IsNullOrWhiteSpace(entry.Value))
                    {
                        Logging.Logger.Info("Empty entry found: {0}", entry.Key);
                    }

                    Logging.Logger.Debug("Entry found: {0}:{1}", entry.Key, entry.Value);

                    result.Add(entry.Key, entry.Value);
                }
            }

            Logging.Logger.Trace("Method Exit, ConfigEntries.Parse: returning {0} entries", result.Count);

            return result;
        }

        private static KeyValuePair<string, string> SplitEntry(string entry)
        {
            if (!entry.Contains(EntryDelimiter))
            {
                return new KeyValuePair<string, string>(String.Empty, String.Empty);
            }

            var splitEntry = entry.Split(EntryDelimiter.ToCharArray(), 2, StringSplitOptions.None);

            return new KeyValuePair<string, string>(splitEntry[0].Trim(), splitEntry[1].Trim());
        }
    }
}
