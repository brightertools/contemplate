﻿using NLog;

namespace ConTemplate.Application
{
    internal static class Logging
    {
        public static Logger Logger { get; private set; }

        static Logging()
        {
            Logger = LogManager.GetLogger("ConTemplateLogger");
        }
    }
}
