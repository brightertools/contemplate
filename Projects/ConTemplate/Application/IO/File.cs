﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ConTemplate.Application.IO
{
    public static class File
    {
        public static string ToString(string filename, bool throwExceptionIfFileDoesNotExist)
        {
            var result = new StringBuilder();

            if (System.IO.File.Exists(filename))
            {
                using (var streamReader = new StreamReader(filename))
                {
                    String line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        result.Append(String.Concat(line, Environment.NewLine));
                    }
                }
            }
            else
            {
                if (throwExceptionIfFileDoesNotExist)
                {
                    throw new IOException("The specified file does not exist");
                }
            }

            return result.ToString();
        }

        /// <summary>
        /// Lists all the files to the specified pattern within the specified path (and subfolders)
        /// </summary>
        /// <param name="path"></param>
        /// <param name="includedExtensions">Comma separated list of extensions eg: .pdf,.jpg or *.* for all</param>
        /// <param name="recursive"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetList(string path, string includedExtensions, bool recursive)
        {
            var queue = new Queue<string>();
            queue.Enqueue(path);
            while (queue.Count > 0)
            {
                path = queue.Dequeue();

                if (recursive)
                {
                    try
                    {
                        foreach (var subDir in Directory.GetDirectories(path))
                        {
                            queue.Enqueue(subDir);
                        }
                    }
                    catch(Exception exception)
                    {
                        Logging.Logger.Error(exception.Message);
                    }
                }

                var files = new List<string>();
                try
                {
                    var allFiles = Directory.GetFiles(path, "*.*");
                    if (String.IsNullOrWhiteSpace(includedExtensions) || includedExtensions == "*.*")
                    {
                        files = allFiles.ToList();
                    }
                    else
                    {
                        var extensions = includedExtensions.Split(',').ToList();
                        foreach (var filenames in extensions.Select(ext => allFiles.Where(x => Equals((Path.GetExtension(x) ?? String.Empty).ToLower(), ext.ToLower())).ToList()).Where(filenames => filenames.Count > 0))
                        {
                            files.AddRange(filenames);
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logging.Logger.Error(exception.Message);
                }

                if (files.Count > 0)
                {
                    foreach (var file in files)
                    {
                        yield return file;
                    }
                }
            }
        }
    }
}