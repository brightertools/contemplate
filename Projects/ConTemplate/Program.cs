﻿using System;
using ConTemplate.Application;

namespace ConTemplate
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Logging.Logger.Info("");
            Logging.Logger.Info("ConTemplate configuration generator Started : {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
            Logging.Logger.Info("");

            var configuration = new Configuration(args);

            if (configuration.Validate())
            {
                ConfigurationFiles.Generate(configuration);
            }
            else
            {
                Logging.Logger.Fatal("Invalid configuration, please check the command line parameters");
            }

            Logging.Logger.Info("");
            Logging.Logger.Info("ConTemplate configuration generator Completed : {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss"));
            Logging.Logger.Info("");
        }
    }
}