ConTemplate - Configuration file Template Generation

An application configuration template system to easily manage multiple configuration files for multiple environments.

For more info, visit: https://contemplate.codeplex.com/

Copyright (c) Brighter Tools Ltd.